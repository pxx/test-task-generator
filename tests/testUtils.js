module.exports.testReturnFn = (sequencer, ...args) => {
    const seq = sequencer(...args);
    expect(typeof seq).toBe('function');
};
