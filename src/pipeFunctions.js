module.exports.accumulator = () => {
    let sum = 0;
    return (number) => {
        if (!Number.isFinite(number)) {
            throw new Error('argument should have type Number');
        }
        sum += number;
        return sum;
    }
};

module.exports.isEven = () => {
    return (number) => {
        if (!Number.isFinite(number)) {
            throw new Error('argument should have type Number');
        }

        return {
            number,
            status: number % 2 === 0
        };
    }
};
