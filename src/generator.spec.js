const generator = require('./generator');
const { dummySeq, factorialSeq, rangeSeq } = require('./sequencers');

describe('Test generator.js', () => {
    describe('generator()', () => {
        test('Throw error if sequencer is not a function', () => {
            const fnErrorMessage = 'sequencer must be a function';
            expect(generator).toThrowError(fnErrorMessage);
            expect(generator.bind(null, 1)).toThrowError(fnErrorMessage);
        });

        test('Generator should have next() method', () => {
            const gen = generator(() => {});
            expect(typeof gen.next).toBe('function');
        });

        test('Generator with factorialSeq() - no additional params', () => {
            const factorialGen = generator(factorialSeq);
            expect(factorialGen.next()).toBe(1);
            expect(factorialGen.next()).toBe(1);
            expect(factorialGen.next()).toBe(2);
            expect(factorialGen.next()).toBe(6);
            expect(factorialGen.next()).toBe(24);
        });

        test('Generator with rangeSeq(strat, step) - pass additional params to sequncer function', () => {
            const rangeGen = generator(rangeSeq, 1, 3);
            expect(rangeGen.next()).toBe(1);
            expect(rangeGen.next()).toBe(4);
            expect(rangeGen.next()).toBe(7);
            expect(rangeGen.next()).toBe(10);
            expect(rangeGen.next()).toBe(13);
        });
    });
});
