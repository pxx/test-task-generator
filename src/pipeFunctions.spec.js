const { testReturnFn } = require('../tests/testUtils');
const { accumulator, isEven } = require('./pipeFunctions');


describe('Test pipeFunctions.js', () => {
    describe('accumulator()', () => {
        test('Throw error if argument is not a number', () => {
            const numberErrorMessage = 'argument should have type Number';
            const acc = accumulator();
            expect(acc).toThrowError(numberErrorMessage);
            expect(acc.bind(null, '2')).toThrowError(numberErrorMessage);
        });

        test('Accumulator sequence', () => {
            const acc = accumulator();
            expect(acc(1)).toBe(1);
            expect(acc(3)).toBe(4);
            expect(acc(4)).toBe(8);
            expect(acc(4)).toBe(12);
            expect(acc(0)).toBe(12);
        });


        test('Return function', () => {
            testReturnFn(accumulator);
        });
    });

    describe('isEven()', () => {
        test('Throw error if argument is not a number', () => {
            const numberErrorMessage = 'argument should have type Number';
            const ie = isEven();
            expect(ie).toThrowError(numberErrorMessage);
            expect(ie.bind(null, '2')).toThrowError(numberErrorMessage);
        });

        test('isEven sequence', () => {
            const ie = isEven();
            expect(ie(-2)).toEqual({ number: -2, status: true });
            expect(ie(-1)).toEqual({ number: -1, status: false });
            expect(ie(0)).toEqual({ number: 0, status: true });
            expect(ie(1)).toEqual({ number: 1, status: false });
            expect(ie(2)).toEqual({ number: 2, status: true });
        });


        test('Return function', () => {
            testReturnFn(isEven);
        });
    });
});
