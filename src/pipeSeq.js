module.exports = function pipeSeq(firstSequencer, ...firstSequencerArgs) {
    if (typeof firstSequencer !== 'function') {
        throw new Error('sequencer must be a function');
    }

    const pipe = [
        firstSequencer(...firstSequencerArgs)
    ];
    let invoked = false;

    const methods = {
        pipeline,
        invoke
    };

    function pipeline(pipeSequencer, ...pipeSequencerArgs) {
        if (typeof pipeSequencer !== 'function') {
            throw new Error('sequencer must be a function');
        }

        if (invoked) {
            throw new Error('It\'s not safe to add sequencers to a pipeline after it has been invoked');
        }

        pipe.push(pipeSequencer(...pipeSequencerArgs));
        return methods;
    }

    function invoke() {
        invoked = true;
        return () => {
            return (initialValue = 0) => {
                return pipe.reduce((result, sequencer) => {
                    return sequencer(result);
                }, initialValue);
            };
        };
    }

    return methods;
};
