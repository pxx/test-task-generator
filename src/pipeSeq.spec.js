const pipeSeq = require('./pipeSeq');
const { factorialSeq, rangeSeq } = require('./sequencers');
const { accumulator, isEven } = require('./pipeFunctions');

describe('Test pipeSeq.js', () => {
    describe('pipeSeq()', () => {
        test('Throw error if first sequencer is not a function', () => {
            const fnErrorMessage = 'sequencer must be a function';
            expect(pipeSeq).toThrowError(fnErrorMessage);
            expect(pipeSeq.bind(null, 1)).toThrowError(fnErrorMessage);
        });

        test('Pipe sequence should have methods: pipeline(), invoke()', () => {
            let piped = pipeSeq(() => {});
            expect(typeof piped.pipeline).toBe('function');
            expect(typeof piped.invoke).toBe('function');
        });

        test('Throw error if sequencer passed to pipeline() method is not a function', ()=> {
            const fnErrorMessage = 'sequencer must be a function';
            const piped = pipeSeq(() => {});
            expect(piped.pipeline).toThrowError(fnErrorMessage);
            expect(piped.pipeline.bind(null, 1)).toThrowError(fnErrorMessage);
        });

        test('pipeline() method should be chainable', () => {
            const piped = pipeSeq(() => {})
                .pipeline(() => {});
            expect(typeof piped.pipeline).toBe('function');
            expect(typeof piped.invoke).toBe('function');
        });

        test('invoke() should return function', () => {
            const piped = pipeSeq(() => {})
                .invoke();

            expect(typeof piped).toBe('function');
        });

        test('Throw error when trying to call pipeline() after invoke()', () => {
            const piped = pipeSeq(() => {})
                .pipeline(() => {});
            piped.invoke();

            expect(piped.pipeline.bind(null, () => {})).toThrowError('It\'s not safe to add sequencers to a pipeline after it has been invoked');
        });

        test('pipeSeq() with 1 seq factorialSeq() - no additional params', () => {
            const piped = pipeSeq(factorialSeq)
                .invoke();
            const seq = piped();
            expect(seq()).toBe(1);
            expect(seq()).toBe(1);
            expect(seq()).toBe(2);
            expect(seq()).toBe(6);
            expect(seq()).toBe(24);
        });

        test('pipeSeq() with 1 seq rangeSeq() - pass additional params to sequncer function', () => {
            const piped = pipeSeq(rangeSeq, 1, 3)
                .invoke();
            const seq = piped();
            expect(seq()).toBe(1);
            expect(seq()).toBe(4);
            expect(seq()).toBe(7);
            expect(seq()).toBe(10);
            expect(seq()).toBe(13);
        });

        test('Chained 2 sequencers', () => {
            const piped = pipeSeq(rangeSeq, 2, 3)
                .pipeline(accumulator)
                .invoke();
            const seq = piped();

            expect(seq()).toBe(2);
            expect(seq()).toBe(7);
            expect(seq()).toBe(15);
            expect(seq()).toBe(26);
        });

        test('Chained 3 sequencers', () => {
            const piped = pipeSeq(rangeSeq, 2, 3)
                .pipeline(accumulator)
                .pipeline(isEven)
                .invoke();
            const seq = piped();

            expect(seq()).toEqual({ number: 2, status: true });
            expect(seq()).toEqual({ number: 7, status: false });
            expect(seq()).toEqual({ number: 15, status: false });
            expect(seq()).toEqual({ number: 26, status: true });
        });

        test('Initial value including default', () => {
            const piped = pipeSeq(accumulator)
                .invoke();
            const seq = piped();

            expect(seq(1)).toBe(1);
            expect(seq(3)).toBe(4);
            expect(seq(4)).toBe(8);
            expect(seq(4)).toBe(12);
            expect(seq(0)).toBe(12);
            expect(seq()).toBe(12);
        });
    });
});
