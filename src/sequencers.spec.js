const { testReturnFn } = require('../tests/testUtils');
const {
    dummySeq,
    factorialSeq,
    fibonacciSeq,
    rangeSeq,
    primeSeq,
    partialSumSeq
} = require('./sequencers');

describe('Test sequencers.js', () => {
    describe('dummySeq()', () => {
        test('dummy', () => {
            const dummy = dummySeq();
            expect(dummy()).toBe('dummy');
            expect(dummy(1)).toBe('dummy');
            expect(dummy('a')).toBe('dummy');
        });

        test('Return function', () => {
            testReturnFn(dummySeq);
        });
    });

    describe('factorialSeq()', () => {
        test('Factorial sequence', () => {
            const factorial = factorialSeq();
            expect(factorial()).toBe(1);
            expect(factorial()).toBe(1);
            expect(factorial()).toBe(2);
            expect(factorial()).toBe(6);
            expect(factorial()).toBe(24);
        });

        test('Return function', () => {
            testReturnFn(factorialSeq);
        });
    });

    describe('fibonacciSeq()', () => {
        test('Fibonacci sequence', () => {
            const fibonacci = fibonacciSeq();
            expect(fibonacci()).toBe(1);
            expect(fibonacci()).toBe(1);
            expect(fibonacci()).toBe(2);
            expect(fibonacci()).toBe(3);
            expect(fibonacci()).toBe(5);
            expect(fibonacci()).toBe(8);
            expect(fibonacci()).toBe(13);
        });

        test('Return function', () => {
            testReturnFn(fibonacciSeq);
        });
    });

    describe('rangeSeq()', () => {
        test('Throw error if start is not a number', () => {
            const startErrorMessage = 'start should be a number';
            expect(rangeSeq).toThrowError(startErrorMessage);
            expect(rangeSeq.bind(null, '1')).toThrowError(startErrorMessage);
        });

        test('Throw error if step is not a number', () => {
            const stepErrorMessage = 'step should be a number';
            expect(rangeSeq.bind(null, 1)).toThrowError(stepErrorMessage);
            expect(rangeSeq.bind(null, 1, '2')).toThrowError(stepErrorMessage);
        });

        test('Range sequence', () => {
            const range = rangeSeq(1, 2);
            expect(range()).toBe(1);
            expect(range()).toBe(3);
            expect(range()).toBe(5);
            expect(range()).toBe(7);
        });


        test('Return function', () => {
            testReturnFn(rangeSeq, 1, 2);
        });
    });

    describe('primeSeq()', () => {
        test('Prime sequence', () => {
            const prime = primeSeq();

            expect(prime()).toBe(2);
            expect(prime()).toBe(3);
            expect(prime()).toBe(5);
            expect(prime()).toBe(7);
            expect(prime()).toBe(11);
            expect(prime()).toBe(13);
            expect(prime()).toBe(17);
        });

        test('Return function', () => {
            testReturnFn(primeSeq);
        });
    });

    describe('partialSumSeq()', () => {
        test('Throw error if 0 arguments', () => {
            expect(partialSumSeq).toThrowError('expected at least one argument');
        });
        test('Throw error if not all arguments are numeric', () => {
            expect(partialSumSeq.bind(null, 1, '2', 3)).toThrowError('all arguments should have type Number');
        });

        test('Partial sum sequence', () => {
            const partialSum = partialSumSeq(1, 3, 7, 2, 0);
            expect(partialSum()).toBe(1);
            expect(partialSum()).toBe(4);
            expect(partialSum()).toBe(11);
            expect(partialSum()).toBe(13);
            expect(partialSum()).toBe(13);
            expect(partialSum).toThrowError('sequence is finished');
        });

        test('Return function', () => {
            testReturnFn(partialSumSeq, 1, 2);
        });
    });
});
