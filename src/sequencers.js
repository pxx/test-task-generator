module.exports.dummySeq = () => {
    return () => 'dummy';
};
// It could be written as one-liner: dummySeq = () => () => 'dummy';
// I don't like it since it's much less readable

module.exports.factorialSeq = () => {
    let [result, index] = [1, 0];
    return () => {
        result *= index || 1;
        index++;
        return result;
    };
};

module.exports.fibonacciSeq = () => {
    let [prev, curr] = [0, 1];
    return () => {
        [prev, curr] = [curr, prev + curr];
        return prev;
    };
};

module.exports.rangeSeq = (start, step) => {
    if (!Number.isFinite(start)) {
        throw new Error('start should be a number');
    }
    if (!Number.isFinite(step)) {
        throw new Error('step should be a number');
    }

    let current = start - step;
    return () => {
        current += step;
        return current;
    };
};

module.exports.primeSeq = () => {
    const primes = [];
    return () => {
        if (!primes.length) {
            primes.push(2);
            return 2;
        }

        const add = (primes.length === 1) ? 1 : 2;
        for (let number = primes[primes.length - 1] + add;; number += 2) {
            if (_isPrime(number, primes)) {
                primes.push(number);
                return number;
            }
        }
    };

    function _isPrime(number, primes) {
        const limit = Math.sqrt(number);
        for (let i = 1, prime = primes[i]; prime <= limit; prime = primes[++i]) {
            if (number % prime === 0) {
                return false;
            }
        }
        return true;
    }
};

module.exports.partialSumSeq = (...args) => {
    if (!args.length) {
        throw new Error('expected at least one argument');
    }
    args.forEach((number) => {
        if (!Number.isFinite(number)) {
            throw new Error('all arguments should have type Number');
        }
    });

    const numbers = args;
    let [index, sum] = [-1, 0];

    return () => {
        index++;
        if (index >= numbers.length) {
            throw new Error('sequence is finished');
        }
        sum += numbers[index];
        return sum;
    };
};
