module.exports = function generator(sequencer, ...args) {
    if (typeof sequencer !== 'function') {
        throw new Error('sequencer must be a function');
    }

    const seq = sequencer(...args);
    return {
        next: (...nextArgs) => {
            return seq(...nextArgs);
        }
    };
};
