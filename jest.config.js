// jest.config.js
module.exports = {
    verbose: true,
    rootDir: './src',
    collectCoverage: true,
    coverageDirectory: '<rootDir>/../coverage'
};
